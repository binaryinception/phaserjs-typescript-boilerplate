# Requirements
- Nodejs
- Typescript (global) `npm install -g typescript`
- Typings (global) `npm install -g typings`
# Setup your project
1. Clone this repository
2. Navigate to the project folder
3. Install dependencies: `npm install --dev`
4. Install description files: `typings install`

# How to build your project
- Compile: `npm run compile` or `tsc`
- Copy dependencies: `npm run copy-dependencies`
- Copy html: `npm run copy-html`
- Do all the above: `npm run build`

# How to test your project
Just start `node simple-server.js` and your game is available on the given port. You can change the port by change the `PORT` environment variable. For example: `PORT=1234 node simple-server.js`.


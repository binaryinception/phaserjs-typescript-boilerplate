var http     = require('http'),
    fs       = require('fs'),
    url      = require('url'),
    path     = require('path'),
    mime     = require('mime'),
    httpPort = process.env.PORT || 8080,
    wwwDir   = './built/';
    

http.createServer(function (req, res) {
    var parsed = url.parse(req.url);

    if (parsed.pathname == '/') {
        parsed.pathname = '/index.html';
    }

    requestedFile = path.join(wwwDir, parsed.pathname);

    if (fs.existsSync(requestedFile)) {
        fs.readFile(requestedFile, (err, data) => {
            if (err) {
                console.error(requestedFile, 'ERROR');

                res.writeHead(500, { 'Content-Type': 'text/plain' });
                res.end(err);

                return;
            }

            console.info(requestedFile, 'OK');
            res.writeHead(200, { 'Content-Type': mime.lookup(requestedFile, 'text/plain') });
            res.end(data);
        });
    } else {
        console.warn(requestedFile, 'NOT FOUND');

        res.writeHead(404, { 'Content-Type' : 'text/plain' });
        res.end(requestedFile + ' not found.');
    }

}).listen(httpPort);

console.info("Listening on", httpPort);